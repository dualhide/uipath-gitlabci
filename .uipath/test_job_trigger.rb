
require './job_trigger.rb'

# you can rename `.env.local.sample_for_testing` to `.env.local` and edit variables for local testing
source `.env.local` rescue nil

# otherwise you can uncomment and edit the variables below
# ENV['ORCHESTRATOR_IDENTIFICATION_URL'] = 'https://cloud.uipath.com/identity_/connect/token'
# ENV['ORCHESTRATOR_APP_URL'] = 'https://cloud.uipath.com/<changeme>/<changeme>/orchestrator_'
# ENV['TEST_SET_ID'] = '<changeme>'
# ENV['ORGANIZATION_UNIT_ID'] = '<changeme>'
# ENV['APP_ID'] = '<changeme>'
# ENV['APP_SECRET'] = '<changeme>'
# ENV['APP_SCOPE'] = '<changeme>'

if __FILE__ == $0
  config = UiPathClient.parse_env_variables()
  UiPathClient.start_test_set(config)
end
