require 'net/http'
require 'uri'
require 'json'

module UiPathClient
  DEFAULT_POLLING_TIMES = '10'
  DEFAULT_POLLING_INTERVAL_SEC = '30'
  def self.parse_env_variables()
    config_map = {
      orchestrator_identification_url: ENV['ORCHESTRATOR_IDENTIFICATION_URL'],
      orchestrator_app_url: ENV['ORCHESTRATOR_APP_URL'],
      test_set_id: ENV['TEST_SET_ID'],
      organization_unit_id: ENV['ORGANIZATION_UNIT_ID'],
      app_id: ENV['APP_ID'],
      app_secret: ENV['APP_SECRET'],
      app_scope: ENV['APP_SCOPE'],
      polling_times: (ENV['POLLING_TIMES'] || UiPathClient::DEFAULT_POLLING_TIMES).to_i,
      polling_interval_sec: (ENV['POLLING_INTERVAL_SEC'] || UiPathClient::DEFAULT_POLLING_INTERVAL_SEC).to_i,
    }
    config_map.each { |k, v|
      raise "no #{k} was found in environment variables..." if v.nil? or v == ''
    }
    return config_map
  end

  def self.start_test_set(config)
    uri_identification = URI.parse(config[:orchestrator_identification_url])
    params = { 
      grant_type: 'client_credentials',
      client_id: config[:app_id],
      client_secret: config[:app_secret],
      scope: config[:app_scope],
    }

    token = get_token(uri_identification, params)

    uri_start_test = URI.parse("#{config[:orchestrator_app_url]}/api/TestAutomation/StartTestSetExecution?testSetId=#{config[:test_set_id]}&triggerType=ExternalTool")
    header = {
      'Content-Type': 'text/json',
      'Authorization': "Bearer #{token}",
      'accept': 'application/json',
      'X-UIPATH-Orchestrator': 'true',
      'X-UIPATH-OrganizationUnitId': config[:organization_unit_id],
    }

    response = http_post(uri_start_test, "", header)
    raise "failed to start test set... test_set_id=#{config[:test_set_id]}" unless response.is_a?(Net::HTTPSuccess)
    puts "test is started... test_set_id=#{config[:test_set_id]}"
    test_set_execution_id = response.body

    uri_check_test_set_execution = URI.parse("#{config[:orchestrator_app_url]}/odata/TestSetExecutions(#{test_set_execution_id})")
    header = {
      'Authorization': "Bearer #{token}",
      'accept': 'application/json',
      'X-UIPATH-Orchestrator': 'true',
      'X-UIPATH-OrganizationUnitId': "#{config[:organization_unit_id]}",
    }

    test_set_exexution_status = nil
    config[:polling_times].times { |i|
      test_set_exexution_status = get_test_execution_status(uri_check_test_set_execution, header)
      if ['Pending', 'Running'].include?(test_set_exexution_status)
        puts "[#{(i+1)}/#{config[:polling_times]}] polling... status=#{test_set_exexution_status}"
        sleep config[:polling_interval_sec]
      else
        puts "[#{(i+1)}/#{config[:polling_times]}] completed with status=#{test_set_exexution_status}"
        break
      end
    }
    raise "Job was finished with status=#{test_set_exexution_status}" unless test_set_exexution_status == 'Passed'
    puts "Completed with status=#{test_set_exexution_status}"
  end

  def self.http_get(uri, header)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = uri.scheme === "https"
  
    http.get(uri.path, header)
  end
  
  def self.http_post(uri, params, header = {'Content-Type': 'text/json'})
    response = nil
    
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = uri.scheme === "https"
  
    path = uri.path
    path += "?#{uri.query}" unless uri.query.nil?
  
    request = Net::HTTP::Post.new(path)
    request.initialize_http_header(header)
  
    if !params.nil? and params.is_a?(Hash)
      request.set_form_data(params)
      return http.request(request)
    end
  
    request.body = params if !params.nil? and params.is_a?(String)
    http.request(request)
  end
  
  def self.get_token(uri_identification, params)
    response = http_post(uri_identification, params)
    raise "failed to get access token... status=#{response.code}, body=#{response.body}" unless response.is_a?(Net::HTTPSuccess)
  
    response_hash = JSON.parse(response.body, symbolize_names: true)
    response_hash[:access_token]
  end
  
  def self.get_test_execution_status(uri, header)
    response = http_get(uri, header)
    raise "failed to get test execution status... status=#{response.code}, body=#{response.body}" unless response.is_a?(Net::HTTPSuccess)
  
    response_hash = JSON.parse(response.body, symbolize_names: true)
    status = response_hash[:Status]
    status
  end
end

if __FILE__ == $0
  config = UiPathClient.parse_env_variables()
  UiPathClient.start_test_set(config)
end
