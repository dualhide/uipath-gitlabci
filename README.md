# UiPath GitLabCI

## Getting started

You can add E2E testing using UiPath TestSuite in your GitLab's CI/CD pipeline with this template.

## How this works

Build-in this template on your project and set paramenters from your UiPath TestSuite.

![flow](./.image/gitlabci.drawio.png)

## Add your files

Copy .uipath direcotry in your workspace and edit your .gitlab-ci.yml. You can understand how to edit to see .gitlab-ci.yml in this project. 

| Environment Variables           | Description                                                 | Sample Value                                             |
|---------------------------------|-------------------------------------------------------------|----------------------------------------------------------|
| APP_ID                          | UiPath's Application ID                                     |                                                          |
| APP_SECRET                      | UiPath's Application Secret                                 |                                                          |
| ORCHESTRATOR_IDENTIFICATION_URL | UiPath Orchestrator identification server's url             | https://cloud.uipath.com/identity_/connect/token         |
| ORCHESTRATOR_APP_URL            | UiPath Orchestrator base url                                | https://cloud.uipath.com/changeme/changeme/orchestrator_ |
| TEST_SET_ID                     | Your test set id in UiPath Orchestrator console             | 9999                                                     |
| ORGANIZATION_UNIT_ID            | UiPath Orchestrator organization unit ID                    | 99999                                                    |
| POLLING_TIMES                   | Polling timeout value for checking E2E testing results      | 10                                                       |
| POLLING_INTERVAL_SEC            | Polling interval sec value for checking E2E testing results | 30                                                       |

